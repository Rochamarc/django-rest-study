from django.contrib.auth.models import User, Group
from rest_framework import serializers

class UserSerializer(serializers.HiperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

class GroupSerializer(serializers.HiperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

